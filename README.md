# Courses store  on vue 3

Pet project to improve practical skills in vue 3 and around

## Данный проект предназначен для командной работы над "красивым" проектом на vue 3 и других мэйнстримовских элементов экосистемы vue 3.

## Стек технологий и инструментов

* vue 3
  * vue-router
  * vuex
  * vue-i18n@next
* jest
* firebase
* node
  * npm
  * eslint
  * vue cli
* Git
* GitLab
* VS Code
  * Prettier VS Code plugin
  * Vetur VS Code plugin
* ...

## Архитектурные ограничения

* Авторизация firebase должна быть максимально инкапсулирована. В идеале должно быть два сервиса авторизации, автоматические перекллючаемые при разных деплоях
